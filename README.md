# 雾霾图像交通标志shibie

#### 介绍
该课题为基于MATLAB bp神经网络的雾霾天气下交通标志的识别系统。主要分两步骤，一是进行图像去雾，采用暗通道的方法获取光透射率，从而去除雾霾。得到清晰的图片后，利用颜色的方法进行交通标志的定位，众所周知，交通标志基本是红，蓝，黄三色组成，根据RGB不同组合可以定位到不同颜色，因为存在误差，所以需要借助形态学相关知识，将得到的误干扰面积去除，从而实现精准定位。定位后，在原图基础上进行分割出彩色图标，利用bp神经网络方法，进行训练，识别，从而得出结果。本设计配有一个GUI可视化界面，操作简单容易上手。是个不错的选题。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
